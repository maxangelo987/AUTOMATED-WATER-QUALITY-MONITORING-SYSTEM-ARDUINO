String inputstring = "";                              //a string to hold incoming data from the PC
String sensorstring = "";                             //a string to hold the data from the Atlas Scientific product
boolean input_string_complete = false;                //have we received all the data from the PC
boolean sensor_string_complete = false;               //have we received all the data from the Atlas Scientific product
float DO;                                             //used to hold a floating point number that is the DO

class DOSensor{
public:
	void do_loop();
};

void DOSensor::do_loop() {                            //here we go...

  if (input_string_complete == true) {                //if a string from the PC has been received in its entirety
    inputstring = "";                                 //clear the string
    input_string_complete = false;                    //reset the flag used to tell if we have received a completed string from the PC
  }

  if (sensor_string_complete == true) {               //if a string from the Atlas Scientific product has been received in its entirety
                                                      //uncomment this section to see how to convert the D.O. reading from a string to a float 
    if (isdigit(sensorstring[0])) {                   //if the first character in the string is a digit
      DO = sensorstring.toFloat();                    //convert the string to a floating point number so it can be evaluated by the Arduino
    }
  
  }
  
 sensorstring = "";                                   //clear the string:
 sensor_string_complete = false;                      //reset the flag used to tell if we have received a completed string from the Atlas Scientific product

 Serial.print(DO);                                    //send that string to the PC's serial monitor
 Serial.println(" ");
}
