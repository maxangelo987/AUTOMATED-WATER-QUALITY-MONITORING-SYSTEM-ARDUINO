float calibration = 26.7; // 27.2 = 4.0
const int analogInPin = A0; 
int sensorValue = 0; 
unsigned long int avgValue; 
float b;
int buf[10],temp;
float phValue;

class PHSensor{
public:
	void ph_loop();
};

void PHSensor::ph_loop() {
 
for(int i=0;i<10;i++){ 
  buf[i]=analogRead(analogInPin);
  delay(10);
 }
 for(int i=0;i<9;i++){
  for(int j=i+1;j<10;j++){
   if(buf[i]>buf[j]){
    temp=buf[i];
    buf[i]=buf[j];
    buf[j]=temp;
  }
 }
}
 
avgValue=0;
for(int i=2;i<8;i++)
avgValue+=buf[i];
float pHVol=(float)avgValue*5.0/1024/6;
phValue = -5.70 * pHVol + calibration;

Serial.print(phValue);
Serial.println(" ");

}
